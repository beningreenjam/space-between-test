# Space Between Test

This is the result of a practical challenge to read Flickr's public API and present it in a given layout. This test was given to me as part of a Frontend Web Developer application.

A live demo is available to view [here](https://test-space-between.netlify.com).

This project is a comprehensive demonstration of my technical abilities to:
- Write clean and efficient components.
- Handling JSONP API responses.
- Write clean and efficient HTML and SCSS that meets the design and interactivity specifications, while maintaining awareness of potential browser compatibility issues.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

#####When deploying in the production environment, serve from the `/dist` directory

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
