export const FlickrItemMixin = {
    computed: {
        transformTitle() {
            return this.item.title.trim().toLowerCase();
        },

        reformatDate() {
            return moment(this.item.date_taken).format('Do MMM YYYY [at] kk:mm');
        }
    }
};